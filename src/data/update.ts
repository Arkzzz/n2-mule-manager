//import { Updater } from '@n2/updater';
const updater = require('@n2/updater');
import * as fs from 'fs';
import * as path from 'path';
import { Logger, LogLevel } from '@n2/common';
export class Updater {
    static update(): Promise<boolean> {
        return new Promise((resolve: (done: boolean) => void, reject: (err: Error) => void) => {
            Promise.all([
                updater.getGroundTypes(),
                updater.getObjects()
            ]).then(([groundTypes, objects]) => {
                fs.createWriteStream(path.join(__dirname, 'groundtypes.json')).end(groundTypes);
                fs.createWriteStream(path.join(__dirname, 'objects.json')).end(objects);
            });
            updater.getRemoteClientVersion().then((version: string) => {
                return updater.getClient(version);
            }).then((client: Buffer) => {
                fs.createWriteStream(path.join(__dirname, 'client.swf')).end(client);
                return updater.unpackSwf(path.join(__dirname, 'client.swf'));
            }).then(() => {
                const packets = updater.extractPacketInfo(fs.readFileSync(updater.makeGSCPath(path.join(__dirname, 'decompiled'))).toString('utf-8'));
                fs.writeFileSync(path.join(__dirname, 'packets.json'), JSON.stringify(packets));
                Logger.log('Updater', 'Sucsessfully updated!', LogLevel.Info);
                resolve(true);
            });
        });
    }
}