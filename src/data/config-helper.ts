import { Logger, LogLevel } from "@n2/common";

const fs = require('fs');
export class ConfigHelper {
    readonly path: string;
    private data: any;
    constructor(path: string) {
        this.path = path;
        this.load();
    }

    public load(): Array<any> | Object {
        try {
            this.data = require(this.path);
        } catch (err) {
            Logger.log('fs', 'Error while loading json.', LogLevel.Error);
            throw new Error(err);
        }
        return this.data;
    }

    public get(): Array<any> | Object {
        return this.data;
    }

    public set(data: Array<any> | Object): void {
        this.data = data;
    }

    public save(data?: Array<any> | Object): void {
        this.data = data;
        fs.writeFile(this.path, JSON.stringify(this.data), (err: string) => {
            if (err) {
                Logger.log('fs', 'Error while saving json.', LogLevel.Error);
                throw new Error(err);
            }
        });
    }
}