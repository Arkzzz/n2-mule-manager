import { Mule } from "../models/mule";

export class MuleUtils {
    static isOptimized(mule: Mule): boolean {
        const hasVaultSpace = mule.vault.indexOf(-1) > -1;
        const hasInvItems = this.amountOfInArray(mule.inventory.slice(4, 12), -1) < 8;
        return !(hasVaultSpace && hasInvItems);
    }

    static amountOfInArray(array: Array<any>, item: any): number {
        let items = 0;
        array.forEach((e) => {
            if (e == item) items++;
        });
        return items;
    }

    static hasSpace(mule: Mule): boolean {
        return (mule.vault.indexOf(-1) > -1 || mule.inventory.slice(4, 12).indexOf(-1) > -1)
    }
}