const VAULT_REGEX = /(?<=<Chest>)(-?\d+,?)+(?=<\/Chest>)/g;
export class VaultParser {
    static parseVaults(xml: string): number[][] {
        let match = VAULT_REGEX.exec(xml);
        let vaults: number[][] = [];
        while (match) {
            const v: number[] = JSON.parse(`[${match[0]}]`);
            while (v.length < 8) {
                v.push(-1);
            }
            vaults.push(v);
            match = VAULT_REGEX.exec(xml);
        }
        if (vaults.length < 1) {
            vaults.push(new Array<number>(8).fill(-1));
        }
        return vaults;
    }
}