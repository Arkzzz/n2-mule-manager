const ERROR_REGEX = /(?<=<Error>)(.*)(?=<\/Error>)/g;
export class ErrorParser {
    static parseError(xml: string): Error {
        let error: Error;
        let match = ERROR_REGEX.exec(xml);
        while (match) {
            error = new Error(match[0]);
            match = ERROR_REGEX.exec(xml);
        }
        return error;
    }
}