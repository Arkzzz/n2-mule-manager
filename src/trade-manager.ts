import { Server } from "@n2/core";
import { Mule } from './models/mule';
import { Trade, TradeType } from './models/trades';
import { Mapper } from "@n2/net";
import { PlayerMuleClient } from "./stdlib/p2m-client";
import { Logger, LogLevel } from "@n2/common";
export class TradeManager {
    /**
     * @param servers Array of `Server` objects.
     */
    private servers: Server[];

    /**
     * @param buildVersion The current build version
     */
    private buildVersion: string;

    private tradesQueue: Trade[];
    
    constructor(buildVersion: string, servers: Server[]) {
        this.buildVersion = buildVersion;
        this.servers = servers;
        Mapper.mapIds(require('./data/packets.json'));
    }
    /**
     * Will return a trade object with the updated mules
     * @param trade Trade to complete
     */
    public addTrade(trade: Trade): Promise<Trade> {
        return new Promise((resolve: (trade: Trade) => void, reject: (err: Error) => void) => {
            if (trade.type == TradeType.PLAYERTOMULE) {
                if (typeof trade.from != 'string') throw new Error('Trade.from must be a Mule for a PLAYERMULE trade.');
                if (typeof trade.to == 'string') throw new Error('Trade.to must be a string for a PLAYERMULE trade.');
                console.log(JSON.stringify(trade));
                const client = new PlayerMuleClient(trade.to, trade.from, trade.server, this.buildVersion);
                Logger.log('Trade Manager', 'Starting player mule trade!', LogLevel.Info);
                client.on('done', (mule: Mule) => {
                    trade.from = mule;
                    client.disconnect();
                    client.io.removeAllListeners();
                    resolve(trade);
                });
                client.on('err', (err) => {
                    client.disconnect();
                    client.io.removeAllListeners();
                    reject(new Error(err));
                });
            }
        });
    }
}