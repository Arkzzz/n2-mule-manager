import { HttpClient, Endpoints, Logger, LogLevel, DefaultLogger } from '@n2/common';
import { Server, ServerParser, AccountParser } from '@n2/core';
import { Mule } from './models/mule';
import { VaultParser } from './parsers/vault-parser';
import { TradeManager } from './trade-manager';
import { ErrorParser } from './parsers/error-parser';
import { EventEmitter } from 'events';
import { Trade, TradeType } from './models/trades';
import { ErrorType } from './models/error-type';
import { ConfigHelper } from './data/config-helper';
import { MuleUtils } from './parsers/mule-utils';

/**
 * A CLI based version of a TradeManager
 */
export class MuleManager {
    /**
     * @param servers Array of `Server` objects.
     */
    servers: Server[];

    /**
     * @param mule Array of mules currently active in the runtime.
     */
    private mules: Mule[];
    
    /**
     * private instance members
     */
    readonly buildVersion: string;

    /**
     * @param afterInitFunctions functions to call after initaliziation.
     */
    private afterInitFunctions: Array<() => void>;

    /**
     * @param reloadDelay delay between char/list requests (in seconds), helps to prevent ip ratelimits.
     */
    private reloadDelay: number;
    
    /**
     * @param tradeManager a `TradeManager` that new trades can be added to.
     */
    private tradeManager: TradeManager;

    private configHelper: ConfigHelper;

    /**
     * Mule Manager
     * @param buildVersion the current RoTMG build version.
     * @param configPath path to a config.json file that contains Mule[]
     */
    constructor(buildVersion: string, configPath: string) {
        /* initalizers */
        this.reloadDelay = 2;
        Logger.addLogger(new DefaultLogger);
        Logger.log('Mule Manager', 'Starting...', LogLevel.Info);
        this.buildVersion = buildVersion;

        this.configHelper = new ConfigHelper(configPath);

        this.mules = this.configHelper.get() as Mule[];
        
        // Get servers
        Logger.log('Mule Manager', 'Fetching servers...', LogLevel.Info);
        this.loadServers().then((servers: Server[]) => {
            this.servers = servers;
            Logger.log('Mule Manager', `Got ${this.servers.length} servers!`, LogLevel.Info);
            this.tradeManager = new TradeManager(this.buildVersion, this.servers);
            // Run after init functions
            this.initalized();
        });
    }

    public addTrade(trade: Trade): Promise<Trade> {
        return new Promise((resolve: (trade: Trade) => void, reject: (err: Error) => void) => {
            if (trade.type == TradeType.PLAYERTOMULE) {
                if (!trade.to) {
                    for (let i = 0; i < this.mules.length; i++) {
                        if (MuleUtils.hasSpace(this.mules[i])) {
                            trade.to = this.mules[i];
                            break;
                        }
                    }
                    if (!trade.to) {
                        reject(new Error('No mules with space'));
                        Logger.log('Mule Manager', 'Could not find a mule with space!', LogLevel.Error);
                    }
                    this.tradeManager.addTrade(trade).then((trade: Trade) => {
                        this.updateFromTrade(trade);
                        resolve(trade);
                    });
                } else {
                    this.tradeManager.addTrade(trade).then((trade: Trade) => {
                        this.updateFromTrade(trade);
                        resolve(trade);
                    });
                }
            }
        });
    }

    public getMules(): Mule[] {
        return this.mules;
    }

    public getServers(): Server[] {
        return this.servers;
    }

    public reloadAll(): Promise<Mule[]> {
        return new Promise((resolve: (mules: Mule[]) => void, reject: (err: Error) => void) => {
            this.reloadMuleArray(this.mules).then((mules) => {
                this.mules = mules;
                this.configHelper.save(mules);
                resolve(mules);
            }, (err) => reject(err));
        });
    }

    public afterInit(method: () => void): void {
        if (!this.afterInitFunctions) {
            this.afterInitFunctions = [];
        }
        this.afterInitFunctions.push(method);
    }

    private updateFromTrade(trade: Trade): void {
        if (typeof trade.from != 'string') {
            for(let i = 0; i < this.mules.length; i++) {
                if (this.mules[i].guid == trade.from.guid) this.mules[i] = trade.from;
            }
        }

        if (typeof trade.to != 'string') {
            for(let i = 0; i < this.mules.length; i++) {
                if (this.mules[i].guid == trade.to.guid) this.mules[i] = trade.to;
            }
        }
        this.configHelper.save(this.mules);
    }

    private loadServers(): Promise<Server[]> {
        return new Promise((resolve: (servers: Server[]) => void, reject: (err: Error) => void) => {
            HttpClient.get(Endpoints.CHAR_LIST, {query: {guid: 'antiratelimit'}}).then((res: Buffer) => {
                resolve(ServerParser.parse(res.toString('utf-8')));
            });
        });
    }

    private reloadMuleArray(mules: Mule[], timeout?: number): Promise<Mule[]> {
        Logger.log('Mule Manager', `Reloading ${mules.length} mules...`, LogLevel.Info);
        return new Promise((resolve: (mules: Mule[]) => void, reject: (err: Error) => void) => {
            setTimeout(() => {
                let timeouts: NodeJS.Timer[] = []
                for(let i = 0; i < mules.length; i++) {
                    timeouts.push(setTimeout(() => {
                        this.reloadMule(mules[i]).then((mule: Mule) => {
                            mules[i] = mule;
                            if (i >= mules.length - 1) {
                                Logger.log('Mule Manager', 'Successfully reloaded mules!', LogLevel.Success);
                                resolve(mules);
                            }
                        }).catch((err) => {
                            if (err) {
                                if (err == ErrorType.INTERNAL) {
                                    Logger.log('Mule Manager', 'Your IP is being ratelimited by Deca, continuing reload in 5 minutes!', LogLevel.Warning);
                                    timeouts.forEach((to) => clearTimeout(to));
                                    this.reloadMuleArray(mules.slice(i), 300/* 300 = 5 min */).then((afterTimeoutMules: Mule[]) => {
                                        for (let n = 0; n < afterTimeoutMules.length; n++) {
                                            mules[i + n] = afterTimeoutMules[n];
                                            if (i + n >= mules.length - 1) {
                                                resolve(mules);
                                            }
                                        }
                                    });
                                }
                                Logger.log('Mule Manager', `Skipping ${mules[i].guid} due to unknown error!`, LogLevel.Warning);
                                if (i >= mules.length - 1) {
                                    Logger.log('Mule Manager', 'Successfully reloaded mules!', LogLevel.Success);
                                    resolve(mules);
                                }
                            }
                        });
                    }, this.reloadDelay * 1000 * i));
                }/*
                let promises: Promise<Mule>[] = [];
                mules.forEach((mule) => promises.push(this.reloadMule(mule)));
                Promise.all(promises).then((data) => resolve(data)).catch((err) => console.log(err));*/
            }, timeout * 1000);
        });
    }
    
    private reloadMule(mule: Mule): Promise<Mule> {
        Logger.log('Mule Manager', `Reloading ${mule.guid}!`, LogLevel.Info);
        return new Promise((resolve: (mule: Mule) => void, reject: (err: Error) => void) => {
            HttpClient.get(Endpoints.CHAR_LIST, {query: {guid: mule.guid, password: mule.password, muleDump: 'true'}}).then((response) => {
                const res = response.toString('utf-8');
                if (ErrorParser.parseError(res)) {
                    console.log(ErrorParser.parseError(res));
                    reject(ErrorParser.parseError(res));
                } else {
                    mule.accountInfo = AccountParser.parseAccountInfo(res);
                    mule.inventory = AccountParser.parseCharacters(res)[0].playerData.inventory;
                    mule.name = AccountParser.parseCharacters(res)[0].playerData.name;
                    mule.vault = VaultParser.parseVaults(res)[0];
                    Logger.log('Mule Manager', `Reloaded ${mule.guid}!`, LogLevel.Success);
                    resolve(mule);
                }
            });
        });
    }
    // Run post init functions
    private initalized(): void {
        if (this.afterInitFunctions && this.afterInitFunctions.length > 0) {
            this.afterInitFunctions.forEach((func: () => void) => func());
            this.afterInitFunctions = null;
        }
    }
}