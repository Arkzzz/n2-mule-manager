const { MuleManager } = require('./dist/mule-manager');
const fs = require('fs');
const path = require('path');
const manager = new MuleManager('X30.0.0', path.join(__dirname, 'mule-config.json'));
manager.afterInit(() => {
    manager.reloadAll().then((mules) => {
        mules.forEach((mule) => {
            console.log('---');
            console.log('name: ' + mule.name);
            console.log('guid: ' + mule.guid);
            console.log('inv: ' + mule.inventory);
            console.log('vault: ' + mule.vault)
        });
    });
});