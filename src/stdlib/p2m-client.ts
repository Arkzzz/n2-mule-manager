import { Client, Account, Server, GameState } from "@n2/core";
import { Mule } from "../models/mule";
import { EventEmitter } from "events";
import { MuleUtils } from "../parsers/mule-utils";
import { PacketType, MapInfoPacket, UpdatePacket, WorldPosData, InvSwapPacket, SlotObjectData, TradeStartPacket, TradeRequestedPacket, FailurePacket, NewTickPacket, ObjectStatusData, TradeChangedPacket, RequestTradePacket, AcceptTradePacket, TradeDonePacket } from "@n2/net";
import { Logger, LogLevel } from "@n2/common";

export class PlayerMuleClient extends Client {
    private emitter: EventEmitter;
    private server: Server;
    
    private chest: ObjectStatusData;
    private requestInterval: NodeJS.Timer;
    private tradeDone: boolean;

    constructor(mule: Mule, player: string, server: Server, buildVersion: string) {
        const account: Account = {guid: mule.guid, password: mule.password, info: mule.accountInfo};
        super(account);

        this.account = account;
        this.server = server;
        this.emitter = new EventEmitter();

        this.io.socket.on('close', () => {
            setTimeout(() => {
                Logger.log(this.account.name, `Reconnecting in 5 seconds!`);
                if (!MuleUtils.isOptimized(mule)) {
                    Logger.log(this.account.name, 'Optimizing mule!', LogLevel.Info);
                    this.connect(this.server, {gameId: -5, buildVersion: buildVersion, characterId: 1} as GameState);
                } else {
                    Logger.log(this.account.name, 'Trading target!', LogLevel.Info);
                    this.connect(this.server, {gameId: -2, buildVersion: buildVersion, characterId: 1} as GameState);
                }
            }); 
        });

        if (!MuleUtils.isOptimized(mule)) {
            Logger.log(this.account.name, 'Optimizing mule!', LogLevel.Info);
            this.connect(this.server, {gameId: -5, buildVersion: buildVersion, characterId: 1} as GameState);
        } else {
            Logger.log(this.account.name, 'Trading target!', LogLevel.Info);
            this.connect(this.server, {gameId: -2, buildVersion: buildVersion, characterId: 1} as GameState);
        }
        
        this.io.on(PacketType.MAPINFO, (mapInfo: MapInfoPacket) => {
            Logger.log(this.account.name, `Connected to ${mapInfo.name}!`, LogLevel.Info);
            if (mapInfo.name == 'Vault' && !MuleUtils.isOptimized(mule)) {
                setTimeout(() => {
                    let delay: number = 0;
                    for(let v = 0; v < mule.vault.length; v++) {
                        if (mule.vault[v] == -1) {
                            for(let i = 3; i < 12; i++) {
                                if ((mule.inventory[i] != -1) && (mule.vault[v] == -1)) {
                                    let invSwap = new InvSwapPacket;
                                    let vaultItem = new SlotObjectData;
                                    let invItem = new SlotObjectData;
                                    vaultItem.objectId = this.chest.objectId;
                                    vaultItem.slotId = v;
                                    vaultItem.objectType = -1;
                                    invSwap.slotObject2 = vaultItem;

                                    invItem.slotId = i;
                                    invItem.objectType = mule.inventory[i];
                                    invItem.objectId = this.objectId;
                                    invSwap.slotObject1 = invItem;

                                    invSwap.position = this.position;

                                    mule.vault[v] = mule.inventory[i];
                                    mule.inventory[i] = -1;
                                    setTimeout(() => {
                                        invSwap.time = this.time;
                                        Logger.log(this.account.name, `Swapping ${invSwap.slotObject1.objectType} in slot ${i} for -1 in vault slot ${v}`, LogLevel.Info);
                                        this.io.send(invSwap);
                                    }, delay);
                                    delay += 600;
                                }
                            }
                        }
                    }
                    setTimeout(() => {
                        console.log(JSON.stringify(mule));
                        Logger.log(this.account.name, 'Optimized!', LogLevel.Success);
                        if (this.tradeDone) {
                            this.disconnect();
                            this.emitter.emit('done', mule);
                        }
                        this.connect(this.server, {gameId: -2, buildVersion: buildVersion, characterId: 1} as GameState);
                    }, delay + 1000);
                }, 3000);
            }
            
            if (mapInfo.name == 'Nexus') {
                const req = new RequestTradePacket;
                req.name = player;
                this.io.send(req);
                this.requestInterval = setInterval(() => {
                    this.io.send(req);
                }, 5000);
            }
        });

        this.io.on(PacketType.UPDATE, (update: UpdatePacket) => {
            if (this.map.name == 'Vault') {
                for(const obj of update.newObjects) {
                    if (obj.objectType == 0x0504) {
                        this.chest = obj.status;
                    }
                    if (obj.status.objectId == this.objectId) {
                        this.position = new WorldPosData(44.5, 70.5);
                    }
                }
            }
        });

        this.io.on(PacketType.TRADESTART, (tradeStart: TradeStartPacket) => {
            clearTimeout(this.requestInterval);
            Logger.log(this.account.name, 'Partner accepted the trade request!', LogLevel.Info);
        });

        this.io.on(PacketType.TRADECHANGED, (tradeChanged: TradeChangedPacket) => {
            // Accept trade on trade changed.
            const accept: AcceptTradePacket = new AcceptTradePacket;
            accept.clientOffer = new Array<boolean>(12).fill(false);
            accept.partnerOffer = tradeChanged.offer;
            this.io.send(accept);
        });
        this.io.on(PacketType.TRADEDONE, (tradeDone: TradeDonePacket) => {
           Logger.log(this.account.name, 'Finished collecting items from player!', LogLevel.Success);
           
           this.tradeDone = true;
           setTimeout(() => {
                mule.inventory = this.stats.inventory;
                if(MuleUtils.isOptimized(mule)) {
                    this.disconnect();
                    this.emitter.emit('done', mule);
                } else {
                    Logger.log(this.account.name, 'Optimizing mule!', LogLevel.Info);
                    this.connect(this.server, {gameId: -5, buildVersion: buildVersion, characterId: 1} as GameState);
                }
           }, 3000);
        });

        this.io.on(PacketType.FAILURE, (failure: FailurePacket) => {
            Logger.log(this.account.name, `Recieved faulure ${failure.errorId}: ${failure.errorDescription}`, LogLevel.Error);
            this.disconnect();
        });
    }

    public on(event: string, listener: (i: any) => void): this {
        this.emitter.on(event, listener);
        return this;
    }
}