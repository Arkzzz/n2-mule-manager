import { Mule } from './mule';
import { Server } from '@n2/core';
/**
 * @interface Trade - Trade between two mules in the system.
 */
export enum TradeType {
    MULETOMULE = 'MULETOMULE',
    PLAYERTOMULE = 'PLAYERTOMULE',
    MULETOPLAYER = 'MULETOPLAYER'
}
export interface Trade {
    /**
     * @param type The type of trade to complete.
     */
    type: TradeType;

    /**
     * @param server The server to complete the trade in.
     */
    server: Server;

    /**
     * @param from The mule or player to get items from.
     */
    from?: Mule | string;

    /**
     * @param to The mule or player to trade the items to, when using `MuleManager` can be set to `null` to automatically sort.
     */
    to?: Mule | string;
    
    /**
     * @param item The item id of the item to move.
     */
    item?: number;
    
    /**
     * @param quantity The quantity of items to trade.
     */
    quantity?: number;

}