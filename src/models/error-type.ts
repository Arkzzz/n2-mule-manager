export enum ErrorType {
    INTERNAL = 'Internal error, please wait 5 minutes to try again!'
}