import { AccountInfo } from '@n2/core'
import { MuleManager } from '../mule-manager';
export interface Mule {
    /**
     * @param guid The guid used when signing into the account.
     */
    guid: string;

    /**
     * @param password The password used when signing into the account.
     */
    password: string;

    /**
     * @param accountInfo `AccountInfo` of the mule.
     */
    accountInfo: AccountInfo;

    /**
     * @param name The in game name of the mule.
     */
    name?: string;

    /**
     * @param inv The inventory data for the mule.
     */
    inventory?: number[];
    /**
     * @param vault The vault data of the mule, this will not be acurate if not properley reloaded/or modified by an outside source.
     */
    vault?: number[];

    /**
     * @param dedicate Will dedicate the mule to a specific item type when using `TradeManager` will be `null` otherwise. 
     */
    dedicate?: number;
}